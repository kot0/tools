package tools

import "runtime"

func GenerateTestTasksForBenchmark(val int, rangeVal int) (tasks []int) {
	for i := val - rangeVal; i < val+rangeVal; i++ {
		tasks = append(tasks, i)
	}
	return
}

func GetCallLocation(skipCalls int) (string, int) {
	_, callFile, callLineNumber, _ := runtime.Caller(skipCalls)
	return callFile, callLineNumber
}
