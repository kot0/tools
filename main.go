package tools

import (
	"bufio"
	"bytes"
	"compress/zlib"
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"sync"
	"time"

	"github.com/spf13/cast"
)

const UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36"
const UserAgentMobile = "Mozilla/5.0 (Linux; Android 13; Pixel 7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Mobile Safari/537.36"

const TimeFormat = `2006-01-02 15:04:05`
const TimeFormatWin = `2006-01-02 15-04-05`
const TimeFormatOnlyTime = `15:04:05`
const TimeFormatWinOnlyTime = `15-04-05`
const TimeFormatOnlyDate = `2006-01-02`
const TimeFormatWinOnlyDate = `2006-01-02`

func ToJson(data any) string {
	returnData, _ := json.Marshal(data)
	return string(returnData)
}

func ParseValueDynamicCompile(text string, reg string) string {
	r := regexp.MustCompile(reg)

	tmp := r.FindStringSubmatch(text)

	if len(tmp) < 2 {
		return ""
	}

	tmp[1] = (" " + tmp[1])[1:] // memory leak fix

	return tmp[1]
}

var regexpCompileCache = make(map[string]*regexp.Regexp)
var m sync.Mutex

func ParseValueStaticCompile(text string, reg string) string {
	var r *regexp.Regexp
	m.Lock()
	if regexpCompileCache[reg] == nil {
		m.Unlock()
		r = regexp.MustCompile(reg)
		m.Lock()
		regexpCompileCache[reg] = r
		m.Unlock()
	} else {
		m.Unlock()
		r = regexpCompileCache[reg]
	}

	tmp := r.FindStringSubmatch(text)

	if len(tmp) < 2 {
		return ""
	}

	tmp[1] = (" " + tmp[1])[1:] // memory leak fix

	return tmp[1]
}

func ParseValuesStaticCompile(text string, reg string) [][]string {
	var r *regexp.Regexp
	m.Lock()
	if regexpCompileCache[reg] == nil {
		m.Unlock()
		r = regexp.MustCompile(reg)
		m.Lock()
		regexpCompileCache[reg] = r
		m.Unlock()
	} else {
		m.Unlock()
		r = regexpCompileCache[reg]
	}

	tmp := r.FindAllStringSubmatch(text, -1)

	var result [][]string

	for _, v := range tmp {
		if len(v) < 2 {
			continue
		}

		v[1] = (" " + v[1])[1:] // memory leak fix

		result = append(result, v[1:])
	}

	return result
}

func Readfiletoarray(path string) ([]string, error) {
	file, err := os.Open(path)
	if CheckError(err) {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	err = scanner.Err()
	if CheckError(err) {
		return nil, err
	}

	return ClearStringSlice(lines), nil
}

func AddLineToFile(path string, text string) {
	f, err := os.OpenFile(path, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if CheckError(err) {
		fmt.Println("error os.OpenFile", err)
	}

	_, err = f.WriteString(text)
	if CheckError(err) {
		fmt.Println("error f.WriteString", err)
	}

	f.Close()
}

func ReadfromConsole() string {
	reader := bufio.NewReader(os.Stdin)
	text, _ := reader.ReadString('\n')
	return text
}

func ToBase64FromString(data string) string {
	return base64.StdEncoding.EncodeToString([]byte(data))
}

func ToBase64FromBytes(data []byte) string {
	return base64.StdEncoding.EncodeToString(data)
}

func FromBase64ToBytes(data string) []byte {
	result, _ := base64.StdEncoding.DecodeString(data)
	return result
}

func FromBase64ToString(data string) string {
	result, _ := base64.StdEncoding.DecodeString(data)
	return string(result)
}

func UrlEncode(s string) string {
	return url.QueryEscape(s)
}

func UniqueSlice[T comparable](s []T) []T {
	var returnSlice []T

	var uniqueMap = make(map[T]struct{})
	for _, element := range s {
		_, value := uniqueMap[element]
		if !value {
			uniqueMap[element] = struct{}{}
			returnSlice = append(returnSlice, element)
		}
	}
	return returnSlice
}

func FileExist(path string) bool {
	_, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false
	}
	return true
}

func ZlibCompress(data []byte) ([]byte, error) {
	var buff bytes.Buffer
	w := zlib.NewWriter(&buff)

	_, err := w.Write(data)
	if CheckError(err) {
		return nil, err
	}

	err = w.Close()
	if CheckError(err) {
		return nil, err
	}

	return buff.Bytes(), nil
}

func ZlibDeCompress(data []byte) ([]byte, error) {
	r, err := zlib.NewReader(bytes.NewBuffer(data))
	if CheckError(err) {
		return nil, err
	}

	deData, err := ioutil.ReadAll(r)
	if CheckError(err) {
		return nil, err
	}

	err = r.Close()
	if CheckError(err) {
		return nil, err
	}

	return deData, nil
}

func TryDeleteFile(file string) {
	_ = os.Remove(file)
	return
}

func TryDeleteFiles(files []string) {
	for _, file := range files {
		TryDeleteFile(file)
	}
}

func GetFilesFromFolder(folder string) ([]string, error) {
	var returnFiles []string

	tmpFiles, err := ioutil.ReadDir(folder)
	if CheckError(err) {
		return nil, err
	}

	for _, f := range tmpFiles {
		returnFiles = append(returnFiles, f.Name())
	}

	return returnFiles, nil
}

func GetFileSize(path string) (int64, error) {
	file, err := os.Open(path)
	if CheckError(err) {
		return 0, err
	}

	fi, err := file.Stat()
	if CheckError(err) {
		return 0, err
	}

	return fi.Size(), nil
}

func ToHexFromString(data string) string {
	return hex.EncodeToString([]byte(data))
}

func ToHexFromBytes(data []byte) string {
	return hex.EncodeToString(data)
}

func FromHexToBytes(data string) []byte {
	returnData, err := hex.DecodeString(data)
	if CheckError(err) {
		return nil
	}
	return returnData
}

func OnErrorPanic(err error) {
	if CheckError(err) {
		panic(err)
	}
}

func CheckError(err error) bool {
	if err != nil {
		return true
	}
	return false
}

func Sha1StringToString(data string) string {
	hasher := sha1.New()
	hasher.Write([]byte(data))
	return ToHexFromBytes(hasher.Sum(nil))
}

func Sha256StringToString(data string) string {
	hasher := sha256.New()
	hasher.Write([]byte(data))
	return ToHexFromBytes(hasher.Sum(nil))
}

func Sha512StringToString(data string) string {
	hasher := sha512.New()
	hasher.Write([]byte(data))
	return ToHexFromBytes(hasher.Sum(nil))
}

func Sha256HashStringToString(data string) string {
	hasher := sha256.New()
	hasher.Write([]byte(data))
	return ToHexFromBytes(hasher.Sum(nil))
}

func Sha256HashStringToBytes(data string) []byte {
	hasher := sha256.New()
	hasher.Write([]byte(data))
	return hasher.Sum(nil)
}

func Sha256HashBytesToBytes(data []byte) []byte {
	hasher := sha256.New()
	hasher.Write(data)
	return hasher.Sum(nil)
}

func Sha256HashBytesToString(data []byte) string {
	hasher := sha256.New()
	hasher.Write(data)
	return ToHexFromBytes(hasher.Sum(nil))
}

func Md5StringToByte(data string) []byte {
	hasher := md5.New()
	hasher.Write([]byte(data))
	return hasher.Sum(nil)
}

func Md5StringToString(data string) string {
	hasher := md5.New()
	hasher.Write([]byte(data))
	return ToHexFromBytes(hasher.Sum(nil))
}

func Md5BytesToString(data []byte) string {
	hasher := md5.New()
	hasher.Write(data)
	return ToHexFromBytes(hasher.Sum(nil))
}

func Md5BytesToBytes(data []byte) []byte {
	hasher := md5.New()
	hasher.Write(data)
	return hasher.Sum(nil)
}

func Md5FileToString(filePath string) (string, error) {
	file, err := os.Open(filePath)
	if CheckError(err) {
		return "", err
	}
	defer file.Close()

	hasher := md5.New()
	_, err = io.Copy(hasher, file)
	if CheckError(err) {
		return "", err
	}

	return ToHexFromBytes(hasher.Sum(nil)), nil
}

func SliceContains[T comparable](s []T, val T) bool {
	for _, element := range s {
		if element == val {
			return true
		}
	}
	return false
}

func ScanFolderRecursive(dirPath string) ([]string, []string, error) {
	var files []string
	var folders []string

	err := filepath.Walk(dirPath, func(path string, f os.FileInfo, err error) error {
		if CheckError(err) {
			return err
		}

		f, err = os.Stat(path)
		if CheckError(err) {
			return err
		}

		fMode := f.Mode()
		if fMode.IsDir() {
			folders = append(folders, path)
		} else if fMode.IsRegular() {
			files = append(files, path)
		}

		return nil
	})
	if CheckError(err) {
		return nil, nil, err
	}

	return files, folders, nil
}

func CopyFile(src, dst string) error {
	in, err := os.Open(src)
	if err != nil {
		return err
	}
	defer in.Close()

	out, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, in)
	if err != nil {
		return err
	}
	return out.Close()
}

func RemoveNewlineCharsFromString(text string) string {
	text = strings.ReplaceAll(text, "\n", "")
	text = strings.ReplaceAll(text, "\r", "")
	return text
}

var sqliteEscapeMap = map[string]string{`\\`: `\\\\`, `'`: `''`, `\\0`: `\\\\0`, `\n`: `\\n`, `\r`: `\\r`, `\x1a`: `\\Z`}

func SQLiteEscape(text string) string {
	for b, a := range sqliteEscapeMap {
		text = strings.ReplaceAll(text, b, a)
	}

	return text
}

var mySQLEscapeMap = map[string]string{"\\": "\\\\", "'": `\'`, "\\0": "\\\\0", "\n": "\\n", "\r": "\\r", `"`: `\"`, "\x1a": "\\Z"}

func MySQLEscape(value string) string {
	for b, a := range mySQLEscapeMap {
		value = strings.ReplaceAll(value, b, a)
	}

	return value
}

var telegramEscapeSlice = []string{"_", "*", "[", "]", "(", ")", "~", "`", ">", "#", "+", "-", "=", "|", "{", "}", ".", "!"}

func TelegramEscape(text string) string {
	for _, char := range telegramEscapeSlice {
		text = strings.ReplaceAll(text, char, `\`+char)
	}

	return text
}

type Logger struct {
	File string
	Name string

	ShowTime           bool
	ShowSourceCodeLine bool
	ShowSourceCodePath bool
}

func (logger Logger) Log(text ...any) {
	var out []string

	if logger.Name != "" {
		out = append(out, "["+logger.Name+"]")
	}

	if logger.ShowTime {
		out = append(out, time.Now().Format(TimeFormat))
	}

	callFile, callLineNumber := GetCallLocation(2)

	if logger.ShowSourceCodePath || logger.ShowSourceCodeLine {
		tmp := ""

		if logger.ShowSourceCodePath {
			tmp += callFile
		}

		if logger.ShowSourceCodeLine {
			if logger.ShowSourceCodePath {
				tmp += ":"
			}

			tmp += cast.ToString(callLineNumber)
		}

		out = append(out, tmp)
	}

	finalLogText := strings.Join(out, " ")

	if logger.ShowTime || logger.Name != "" || logger.ShowSourceCodeLine || logger.ShowSourceCodePath {
		finalLogText += ": "
	}

	finalLogText += strings.TrimSuffix(fmt.Sprintln(text...), "\n")

	fmt.Println(finalLogText)

	if logger.File != "" {
		AddLineToFile(logger.File, finalLogText+"\n")
	}
}

func FormatElementsToText(text ...any) string {
	return strings.TrimSuffix(fmt.Sprintln(text...), "\n")
}

func ClearStringSlice(s []string) (returnSlice []string) {
	for _, element := range s {
		element = RemoveNewlineCharsFromString(element)
		if element == "" {
			continue
		}
		returnSlice = append(returnSlice, element)
	}
	return
}

func ReverseSlice[T any](s []T) []T {
	var returnSlice = make([]T, len(s))
	copy(returnSlice, s)

	for i, j := 0, len(returnSlice)-1; i < j; i, j = i+1, j-1 {
		returnSlice[i], returnSlice[j] = returnSlice[j], returnSlice[i]
	}
	return returnSlice
}

func AddPrefixToStringSlice(s []string, prefix string) (returnSlice []string) {
	for _, element := range s {
		returnSlice = append(returnSlice, prefix+element)
	}
	return
}

func ChunkSlice[T any](slice []T, chunkSize int) (chunks [][]T) {
	for chunkSize < len(slice) {
		slice, chunks = slice[chunkSize:], append(chunks, slice[0:chunkSize:chunkSize])
	}
	return append(chunks, slice)
}

func EncloseTextInBrackets(text string) string {
	return "(" + text + ")"
}

func Find2Substr(s, substr1, substr2 string) string {
	index1 := strings.Index(s, substr1)
	if index1 == -1 {
		return ""
	}
	index2 := strings.Index(s[index1:], substr2)
	if index2 == -1 {
		return ""
	}
	// fmt.Println("index1:", index1)
	// fmt.Println("index2:", index2)

	return s[index1+len(substr1) : index1+index2]
}

func MeasureFuncExecTime(f func()) time.Duration {
	tStart := time.Now()
	f()
	return time.Since(tStart)
}

func PrintFuncExecTime(name string, f func()) time.Duration {
	execTime := MeasureFuncExecTime(f)
	fmt.Println("["+name+"]", "took:", execTime)
	return execTime
}
