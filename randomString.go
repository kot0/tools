package tools

const (
	randomStringLetterBytes   = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	randomStringLetterIdxBits = 6                                // 6 bits to represent a letter index
	randomStringLetterIdxMask = 1<<randomStringLetterIdxBits - 1 // All 1-bits, as many as randomStringLetterIdxBits
	randomStringLetterIdxMax  = 63 / randomStringLetterIdxBits   // # of letter indices fitting in 63 bits
)

// GetRandomCharsAsByte returns random []byte with length you provided
func (random *Random) GetRandomCharsAsByte(n int) []byte {
	b := make([]byte, n)
	for i, cache, remain := n-1, random.randomSource.Int63(), randomStringLetterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = random.randomSource.Int63(), randomStringLetterIdxMax
		}
		if idx := int(cache & randomStringLetterIdxMask); idx < len(randomStringLetterBytes) {
			b[i] = randomStringLetterBytes[idx]
			i--
		}
		cache >>= randomStringLetterIdxBits
		remain--
	}

	return b
}
