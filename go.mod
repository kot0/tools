module gitlab.com/kot0/tools

go 1.23.3

require (
	github.com/Pallinder/go-randomdata v1.2.0
	github.com/spf13/cast v1.7.0
)

require golang.org/x/text v0.3.7 // indirect
