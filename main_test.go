package tools

import (
	"testing"
)

func TestRandomStringFast(t *testing.T) {
	tests := []int{
		1,
		10,
		100,
		1000,
		10000,
		100000,
		1000000,
		10000000,
	}

	random := NewRandom()

	for _, test := range tests {
		for _, generatedStringLen := range GenerateTestTasksForBenchmark(test, 150) {
			if generatedStringLen < 1 {
				continue
			}

			if got := random.StringFast(generatedStringLen); len(got) != generatedStringLen {
				t.Errorf("StringFast() = %v, want %v", len(got), generatedStringLen)
			}
		}
	}
}

func TestRandomStringSlow(t *testing.T) {
	tests := []int{
		1,
		10,
		100,
		1000,
		10000,
		100000,
		1000000,
		10000000,
	}

	for _, test := range tests {
		for _, generatedStringLen := range GenerateTestTasksForBenchmark(test, 150) {
			if generatedStringLen < 1 {
				continue
			}

			if got := RandomStringSlow(generatedStringLen); len(got) != generatedStringLen {
				t.Errorf("RandomStringSlow() = %v, want %v", len(got), generatedStringLen)
			}
		}
	}
}

func BenchmarkRandomStringFast(b *testing.B) {
	random := NewRandom()

	for i := 0; i < b.N; i++ {
		random.StringFast(100000)
		b.SetBytes(100000)
	}
}

func BenchmarkRandomStringSlow(b *testing.B) {
	for i := 0; i < b.N; i++ {
		RandomStringSlow(100000)
		b.SetBytes(100000)
	}
}

func BenchmarkFind2Substr(b *testing.B) {
	for i := 0; i < b.N; i++ {
		result := Find2Substr("764376267432data_abc_bata5843856543843", "data", "bata")
		if result == "" {
			panic(`result == ""`)
		}
	}
}

func TestFind2Substr(t *testing.T) {
	type args struct {
		s       string
		substr1 string
		substr2 string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "1",
			args: args{
				s:       "764376267432data_abc_bata5843856543843",
				substr1: "data",
				substr2: "bata",
			},
			want: "_abc_",
		},
		{
			name: "2",
			args: args{
				s:       "764376267432hdsairiydsv_abc_abiudnsvbs7435843856543843",
				substr1: "432hdsairiydsv",
				substr2: "abiudnsvbs7435",
			},
			want: "_abc_",
		},
		{
			name: "3",
			args: args{
				s:       RandomStringSlow(128) + "data_abc_bata" + RandomStringSlow(128),
				substr1: "data",
				substr2: "bata",
			},
			want: "_abc_",
		},
		{
			name: "4",
			args: args{
				s:       "dahta_abc_bafta",
				substr1: "data",
				substr2: "bata",
			},
			want: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Find2Substr(tt.args.s, tt.args.substr1, tt.args.substr2); got != tt.want {
				t.Errorf("Find2Substr() = %v, want %v", got, tt.want)
			}
		})
	}
}
