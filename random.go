package tools

import (
	"math/rand"
	"time"

	"github.com/Pallinder/go-randomdata"
)

type Random struct {
	randomSource rand.Source
}

func NewRandom() *Random {
	return &Random{randomSource: rand.NewSource(time.Now().UnixNano())}
}

// StringFast NOT THREAD SAFE
func (random *Random) StringFast(stringLen int) string {
	if stringLen < 1 {
		return ""
	}
	return string(random.GetRandomCharsAsByte(stringLen))
}

func RandomStringSlow(stringLen int) string {
	if stringLen < 1 {
		return ""
	}
	return randomdata.RandStringRunes(stringLen)
}
